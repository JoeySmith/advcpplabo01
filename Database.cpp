#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdexcept>
#include "Database.h"

using namespace std;

namespace Records {

	Database::Database() : mNextEmployeeNumber(kFirstEmployeeNumber)
	{
	}

  bool Database::addEmployee2( Employee& e) 
  {
    int n =  e.getEmployeeNumber()  ;
    bool ok =  !isDuplicate( n ) ;
    if ( ok ) {
      mEmployees2.emplace_back ( &e ); // xxx
    } else {
      cout << "Find duplicate employee number: " << n << "!!!" << endl;
    } 
    return ok;
  };

  unique_ptr<Employee>& Database::addEmployee2(const string& firstName, const string& lastName, bool hourly )
  {
    int id = mNextEmployeeNumber++ ;
    Employee* e = nullptr ;
    if ( hourly ) {
       e = new HourlyEmployee (firstName, lastName, id, 10, 1.5 );
       mEmployees2.emplace_back( e );
    }  else {
       e = new SalaryEmployee (firstName, lastName , id );
       mEmployees2.emplace_back( e );
    }
   
    return mEmployees2.back();
  }

	const Employee* Database::getEmployee(int employeeNumber)
	{
		for (const auto& e : mEmployees2) {
			if (e->getEmployeeNumber() == employeeNumber) {
				return e.get();
			}
		}
		throw runtime_error("No employee found.");
		return nullptr;
	}

	const auto&     Database::getEmployee(const string& firstName, const string& lastName)
	{
		for (auto& e : mEmployees2) {
			if (e->getFirstName() == firstName && e->getLastName() == lastName) {
					return e      ;
			}
		}
		throw runtime_error("No employee found.");
	}


  void Database::displayAll() const
  {
    for (const auto& e : mEmployees2) {
      cout << *e ;
    }
  }

/*
1000:     Wallis, Greg,   Former,       Wage: $30000
1001:   Gregoire, Marc,  Current,       Wage: $100000
1002:        Doe, John,  Current,     Salary: $11000
*/

  void Database::saveDB( const std::string& fn ) const
  {

    std::filebuf fb;
    fb.open (fn.c_str(),std::ios::out);
    std::ostream os(&fb);
    int n = 12;

    os << "#"  ;
    os << setw(n ) << "Emp No"  ;
    os << setw(n ) << "Last Name"  ;
    os << setw(n ) << "First Name"  ;
    os << setw(n ) << "Status"  ;
    os << setw(n ) << "Type" ;
    os << setw(n ) << "Annual " ;
    os << endl;
    os << "#"  ;
    os << std::string(n*6-1, '-') ;
    os << endl;
    for (const auto& e : mEmployees2) {
      // cout << *e ;
      os << *e ;
    }
    fb.close();
  }

	void Database::saveBinaryDB(const std::string& fn) const
	{
    fstream db(fn, ios::out | ios::binary);
    int cnt = mEmployees2.size() ;

    db.write(reinterpret_cast<const char *> (&cnt), sizeof(cnt));

    for (unsigned i=0; i < cnt; ++i ) {
      Employee* e = mEmployees2[i].get() ;
      bool hourly = typeid(*e).name() == string( "N7Records14HourlyEmployeeE" );

      db.write(reinterpret_cast<const char *> (&hourly), sizeof(hourly));

      if ( hourly ) {
        HourlyEmployee* f=  dynamic_cast<HourlyEmployee* > ( mEmployees2[i].get()) ;
        db.write(reinterpret_cast<const char *> (f), sizeof(*f));
      } else {
        db.write(reinterpret_cast<const char *> (e), sizeof(*e));
      }
		}
    cout << setw(10) << "Save DB: " << cnt << " entries." << endl;
    db.close();
  }

	void Database::readDB2( const string& fn ) 
	{
    fstream fin ( fn, ios::in | ios::binary);

    if ( !fin ) {
      cout << "Could not find the DB file " << fn << endl;
      return ;
    }
    int cnt = 0 ;
    fin.read(reinterpret_cast<char *> (&cnt), sizeof(cnt));
    cout << "Read DB: file=" <<  fn << ", " << cnt << " entries." << endl;

    int tot = 0 ;
    bool ok ;
	  for ( size_t i=0; i < cnt; ++i ) {	
      bool hourly ;
      fin.read(reinterpret_cast<char *> (&hourly), sizeof( hourly ));
	    
      if ( hourly ) {
	      HourlyEmployee *x = new HourlyEmployee() ;
        fin.read(reinterpret_cast<char *> (x), sizeof( *x ));
        ok = addEmployee2( *x); 
      } else {
	      Employee *x = new Employee() ;
        fin.read(reinterpret_cast<char *> (x), sizeof( *x ));
        ok = addEmployee2( *x); 
      }
      if (ok) tot++;
		} 

    fin.close();
    cout << "Add " << tot << " employee's" << endl;
	}

	bool Database::isDuplicate(int id) const
	{
		for (const auto& e : mEmployees2) {
			if ( e->getEmployeeNumber() == id ) {
        return true;
      }
		}
    return false;
	}

	void Database::displayCurrent() const
	{
		for (const auto& e : mEmployees2) {
			if (e->getIsHired())
        cout << *e ;
		}
	}

	void Database::displayFormer() const
	{
		for (const auto& e : mEmployees2) {
			if (!e->getIsHired())
        cout << *e ;
		}
	}

}
